﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  Model class that defines all stats related to a hex
///  -grid position
///  -world position
///  -neighbours
/// </summary>
public class Hex
{

    public Hex(int c, int r) {
        this.C = c;
        this.R = r;
        this.S = -(c + r);

        Radius = 1f;
    }

    //Q + R + S == 0
    //S = -(Q + R)

    public readonly int C; //column index
    public readonly int R; //row index
    public readonly int S; //sum
    public float Radius { get; set; } //hex radius, defaults to 1

    private static readonly float WIDTH_MULTIPLIER = Mathf.Sqrt(3) / 2;

    /// <returns> The world space coordinates of this hex </returns>
    public Vector3 Position() {
        float height = Radius * 2;                  //height = diameter since we are using pointy tops
        float width = Mathf.Sqrt(3) / 2 * height;   //width = some genie shit, apparently it's just how hexes work

        float v_spacing = height * 0.75f;           //hexes do not offset vertically by a whole hex, rather they go up by 3/4 and a bit to the right
        float h_spacing = width;

        return new Vector3(
            h_spacing * (this.C + this.R/2f),
            0,
            v_spacing * this.R
            );
    } 
}
