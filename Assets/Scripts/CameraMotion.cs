﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMotion : MonoBehaviour
{
    //camera speed in units per second (a tile is 1 unit)
    [SerializeField]
    private int tSpeed = 15;
    public int TranslationSpeed { get => tSpeed; set => tSpeed = value; }

    [SerializeField]
    private int zSpeed = 5;
    public int ZoomSpeed { get => zSpeed; set => zSpeed = value; }

    // Update is called once per frame
    void Update()
    {
        //make sure zooming wouldn't put us through the map (may need to be revised based on tile heights and such)
        //invert zSpeed as MouseDown is -ve -> scroll up is +ve
        int zMotion = (transform.position.y > (Input.GetAxis("Mouse ScrollWheel") * zSpeed) ? -zSpeed : 0);

        //Movement
        this.transform.Translate(new Vector3(
            Input.GetAxis("Horizontal") * Time.deltaTime * tSpeed,      //translateX
            Input.GetAxis("Mouse ScrollWheel") * zMotion,               //Zoom (no deltaTime here because it it not a button that gets held down)
            Input.GetAxis("Vertical") * Time.deltaTime * tSpeed         //translateY
        ), Space.World);
        
    }
}
