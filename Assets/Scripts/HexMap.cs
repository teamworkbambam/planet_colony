﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexMap : MonoBehaviour
{
    //These should be constant but I left them this way so they could be modified from the unity editor
    //Please note that changing them programmatically will have no effect, they are never used after instantiation
    public int MAP_WIDTH = 10;
    public int MAP_HEIGHT = 10;

    [SerializeField]
    private GameObject HexPrefab;

    // Start is called before the first frame update
    void Start()
    {
        GenerateMap();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GenerateMap() {
        for (int row = 0; row < MAP_WIDTH; row++)
        {
            for (int col = 0; col < MAP_HEIGHT; col++)
            {
                Hex h = new Hex(col, row);
                //instantiate a hex
                Instantiate(HexPrefab, h.Position(), Quaternion.identity, this.transform);
            }
        }
    }
}
